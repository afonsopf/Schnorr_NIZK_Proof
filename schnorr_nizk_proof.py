import secrets 
import sys

"""
Default group parameters.
Since Schnorr NIZK proof operates over the same cyclic groups as DSA, the same pre-computed group parameters are being used.
The values for L and N (dimension in bits for p and q respectively) are the ones recommended by NIST to use in DSA. (1024, 160 or 2048, 256 or 3072, 256)
All the values can be found in https://csrc.nist.gov/csrc/media/projects/cryptographic-standards-and-guidelines/documents/examples/dsa2_all.pdf
"""
p_1024_160 = 0xE0A67598CD1B763BC98C8ABB333E5DDA0CD3AA0E5E1FB5BA8A7B4EABC10BA338FAE06DD4B90FDA70D7CF0CB0C638BE3341BEC0AF8A7330A3307DED2299A0EE606DF035177A239C34A912C202AA5F83B9C4A7CF0235B5316BFC6EFB9A248411258B30B839AF172440F32563056CB67A861158DDD90E6A894C72A5BBEF9E286C6B
q_1024_160 = 0xE950511EAB424B9A19A2AEB4E159B7844C589C4F
g_1024_160 = 0xD29D5121B0423C2769AB21843E5A3240FF19CACC792264E3BB6BE4F78EDD1B15C4DFF7F1D905431F0AB16790E1F773B5CE01C804E509066A9919F5195F4ABC58189FD9FF987389CB5BEDF21B4DAB4F8B76A055FFE2770988FE2EC2DE11AD92219F0B351869AC24DA3D7BA87011A701CE8EE7BFE49486ED4527B7186CA4610A75

p_2048_256 = 0xC196BA05AC29E1F9C3C72D56DFFC6154A033F1477AC88EC37F09BE6C5BB95F51C296DD20D1A28A067CCC4D4316A4BD1DCA55ED1066D438C35AEBAABF57E7DAE428782A95ECA1C143DB701FD48533A3C18F0FE23557EA7AE619ECACC7E0B51652A8776D02A425567DED36EABD90CA33A1E8D988F0BBB92D02D1D20290113BB562CE1FC856EEB7CDD92D33EEA6F410859B179E7E789A8F75F645FAE2E136D252BFFAFF89528945C1ABE705A38DBC2D364AADE99BE0D0AAD82E5320121496DC65B3930E38047294FF877831A16D5228418DE8AB275D7D75651CEFED65F78AFC3EA7FE4D79B35F62A0402A1117599ADAC7B269A59F353CF450E6982D3B1702D9CA83
q_2048_256 = 0x90EAF4D1AF0708B1B612FF35E0A2997EB9E9D263C9CE659528945C0D
g_2048_256 = 0xA59A749A11242C58C894E9E5A91804E8FA0AC64B56288F8D47D51B1EDC4D65444FECA0111D78F35FC9FDD4CB1F1B79A3BA9CBEE83A3F811012503C8117F98E5048B089E387AF6949BF8784EBD9EF45876F2E6A5A495BE64B6E770409494B7FEE1DBB1E4B2BC2A53D4F893D418B7159592E4FFFDF6969E91D770DAEBD0B5CB14C00AD68EC7DC1E5745EA55C706C4A1C5C88964E34D09DEB753AD418C1AD0F4FDFD049A955E5D78491C0B7A2F1575A008CCD727AB376DB6E695515B05BD412F5B8C2F4C77EE10DA48ABD53F5DD498927EE7B692BBBCDA2FB23A516C5B4533D73980B2A3B60E384ED200AE21B40D273651AD6060C13D97FD69AA13C5611A51B9085

p_3072_256 = 0x90066455B5CFC38F9CAA4A48B4281F292C260FEEF01FD61037E56258A7795A1C7AD46076982CE6BB956936C6AB4DCFE05E6784586940CA544B9B2140E1EB523F009D20A7E7880E4E5BFA690F1B9004A27811CD9904AF70420EEFD6EA11EF7DA129F58835FF56B89FAA637BC9AC2EFAAB903402229F491D8D3485261CD068699B6BA58A1DDBBEF6DB51E8FE34E8A78E542D7BA351C21EA8D8F1D29F5D5D15939487E27F4416B0CA632C59EFD1B1EB66511A5A0FBF615B766C5862D0BD8A3FE7A0E0DA0FB2FE1FCB19E8F9996A8EA0FCCDE538175238FC8B0EE6F29AF7F642773EBE8CD5402415A01451A840476B2FCEB0E388D30D4B376C37FE401C2A2C2F941DAD179C540C1C8CE030D460C4D983BE9AB0B20F69144C1AE13F9383EA1C08504FB0BF321503EFE43488310DD8DC77EC5B8349B8BFE97C2C560EA878DE87C11E3D597F1FEA742D73EEC7F37BE43949EF1A0D15C3F3E3FC0A8335617055AC91328EC22B50FC15B941D3D1624CD88BC25F3E941FDDC6200689581BFEC416B4B2CB73
q_3072_256 = 0xCFA0478A54717B08CE64805B76E5B14249A77A4838469DF7F7DC987EFCCFB11D
g_3072_256 = 0x5E5CBA992E0A680D885EB903AEA78E4A45A469103D448EDE3B7ACCC54D521E37F84A4BDD5B06B0970CC2D2BBB715F7B82846F9A0C393914C792E6A923E2117AB805276A975AADB5261D91673EA9AAFFEECBFA6183DFCB5D3B7332AA19275AFA1F8EC0B60FB6F66CC23AE4870791D5982AAD1AA9485FD8F4A60126FEB2CF05DB8A7F0F09B3397F3937F2E90B9E5B9C9B6EFEF642BC48351C46FB171B9BFA9EF17A961CE96C7E7A7CC3D3D03DFAD1078BA21DA425198F07D2481622BCE45969D9C4D6063D72AB7A0F08B2F49A7CC6AF335E08C4720E31476B67299E231F8BD90B39AC3AE3BE0C6B6CACEF8289A2E2873D58E51E029CAFBD55E6841489AB66B5B4B9BA6E2F784660896AFF387D92844CCB8B69475496DE19DA2E58259B090489AC8E62363CDF82CFD8EF2A427ABCD65750B506F56DDE3B988567A88126B914D7828E2B63A6D7ED0747EC59E0E0A23CE7D8A74C1D2C2A7AFB6A29799620F00E11C33787F7DED3B30E1A22D09F1FBDA1ABBBFBF25CAE05A13F812E34563F99410E73B




class Schnorr_nizk_proof:
    
    def __init__(self, hashFunc, L, N, x_public=None):
        if L == 1024 and N == 160:
            self.p = p_1024_160
            self.q = q_1024_160
            self.g = g_1024_160
        elif L == 2048 and N == 256:
            self.p = p_2048_256
            self.q = q_2048_256
            self.g = g_2048_256
        elif L == 3072 and N == 256:
            self.p = p_3072_256
            self.q = q_3072_256
            self.g = g_3072_256
        else:
            raise Exception("Invalid dimensions for group parameters")
           
        self.hashFunc = hashFunc
        self.x_public = x_public
       
        self._v_private = None
        self.v_public = None
        
    """
    Auxiliary functions egcd and modinv, to compute the modular multiplicative inverse,
    required to compute the modular exponentiation with negative exponents.
    """
    def _xgcd(self, b, n):
        x0, x1, y0, y1 = 1, 0, 0, 1
        while n != 0:
            q, b, n = b // n, n, b % n
            x0, x1 = x1, x0 - q * x1
            y0, y1 = y1, y0 - q * y1
        return  b, x0, y0

    def _modinv(self,a, m):
        g, x, y = self._xgcd(a, m)
        if g != 1:
            raise Exception("Modular inverse does not exist")
        else:
            return x % m
            
            
    """
    Select a private v at random from [1, q-1] and compute the corresponding public value g^v mod p
    """
    def _computeV(self):
        while(self._v_private == None or self._v_private == 0):
            self._v_private = secrets.randbelow(self.q)
            
        self.v_public = pow(self.g, self._v_private, self.p)
        return
        
    """
    Compute the challenge h
    As recommended in the paper, each field of the challenge is prepended with a 4 byte integer indicating its size
    """
    def _computeChallenge(self, uid, otherInfo=None):
        challengeString = (sys.getsizeof(self.g)).to_bytes(4,'big')
        challengeString += (self.g).to_bytes(sys.getsizeof(self.g),'big')
        
        challengeString += (sys.getsizeof(self.v_public)).to_bytes(4,'big')
        challengeString += (self.v_public).to_bytes(sys.getsizeof(self.v_public),'big')
        
        challengeString += (sys.getsizeof(self.x_public)).to_bytes(4,'big')
        challengeString += (self.x_public).to_bytes(sys.getsizeof(self.x_public),'big')
        
        challengeString += (sys.getsizeof(uid)).to_bytes(4,'big')
        challengeString += uid

        if otherInfo != None:
            challengeString += (sys.getsizeof(otherInfo)).to_bytes(4,'big')
            challengeString += otherInfo
            
        return self.hashFunc(challengeString)
        
    """
    Issue the proof for the x_private private exponent passed as input.
    """
    def prove(self, x_private, uid, otherInfo=None):
        if self.x_public == None:
            raise Exception("Public value X not defined")
        if self.v_public == None:
            self._computeV()
        
        proof = {}
        proof["UserID"] = uid
        proof["OtherInfo"] = otherInfo
        proof["V"] = self.v_public
        
        h = int.from_bytes(self._computeChallenge(uid,otherInfo),'big')
        
        r = self._v_private - ((x_private * h) % self.q)
        
        proof["r"] = r
        
        return proof
        
    """
    Verify the proof.
    If the group parameters are correct and the proof was issued with the right private exponent, return true. Else, raise excetion.
    """
    def verify(self, proof):
        if self.x_public == None:
            raise Exception("Public value X not defined")
        if self.x_public < 1 or self.x_public > self.p or pow(self.x_public, self.q, self.p) != 1:
            raise Exception("Public key is not valid for the defined group parameters")
            
        
        uid = proof["UserID"]
        otherInfo = proof["OtherInfo"]
        self.v_public = proof["V"]
        r = proof["r"]
        
        h = int.from_bytes(self._computeChallenge(uid,otherInfo),'big')
        
     
        if r<0:
            verification_number = pow(self._modinv(self.g,self.p),r*(-1),self.p) * pow(self.x_public,h,self.p) % self.p
        else:
            verification_number = pow(self.g,r,self.p) * pow(self.x_public,h,self.p) % self.p
        
        if self.v_public == verification_number:
            return True
        else:
            raise Exception("Verification failed")
        
        
        
