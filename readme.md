# Document under review

### 
Class Schnorr_nizk_proof can be instantiated with the values of L and N (dimension in bits for p and q respectively) recommended by NIST: (1024, 160), (2048, 256) or (3072, 256). Any different pais of values will raise an error.

The actual group parameters used were computed by NIST and were taken from https://csrc.nist.gov/csrc/media/projects/cryptographic-standards-and-guidelines/documents/examples/dsa2_all.pdf
