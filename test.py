from schnorr_nizk_proof import *
from Crypto.Hash import SHA256
import secrets

"""
An hash function to compute the challenge.
I'm using SHA256 implemented in the PyCryptoDome library, but any secure cryptographic hash function would do.
"""
def hashFunc(dataToHash):
    func = SHA256.new()
    func.update(dataToHash)
    return func.digest()

#Instantiate the classes of both the prover and the verifier
alice = Schnorr_nizk_proof(hashFunc, 2048, 256)
bob = Schnorr_nizk_proof(hashFunc, 2048, 256)

#Choose private key for alice, and compute public key with the defined group parameters
x_private = secrets.randbelow(alice.q)
x_public = pow(alice.g, x_private, alice.p)

#Both the prover and the verifier must have knowledge of the public key
alice.x_public = x_public
bob.x_public = x_public

#Issue the proof
alices_proof = alice.prove(x_private,(1).to_bytes(4,'big'), b'Other info can be any byte string')

#Verify if the proof is valid
try:
    print("Alice's verification: ", bob.verify(alices_proof))
except Exception as e:
    print("Alice's verification: ", e)

    
#Oscar is trying to impersonate Alice, so he instantiates a class with the same group parameters and public key.
oscar = Schnorr_nizk_proof(hashFunc, 2048, 256)
oscar.x_public = x_public

#But he doesn't know Alice's private exponent, so he picks one at random
oscars_proof = oscar.prove(secrets.randbelow(oscar.q), (1).to_bytes(4,'big'), b'Other info can be any byte string')

#Verification will fail
try:
    print("Oscar's verification: ", bob.verify(oscars_proof))
except Exception as e:
    print("Oscar's vefification: ", e)
